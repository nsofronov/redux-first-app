import {connect} from 'react-redux'
import {toggleTodo} from '../actions/index'
import TodoList from '../components/TodoList'

const getVisibleTodos = (todos, filter) => {
	switch (filter) {
		case 'completed':
			return todos.filter(t => t.completed);
		case 'active':
			return todos.filter(t => !t.completed)
		default:
		case 'all':
			return todos;
	}
};

const mapStateToProps = (state, ownProps) => {
	console.log(ownProps)
	return {
		todos: getVisibleTodos(state.todos, ownProps.filter)
		// todos: getVisibleTodos(state.todos, state.visibilityFilter)
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onTodoClick: (id) => {
			dispatch(toggleTodo(id))
		}
	}
};

const VisibleTodoList = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoList);

export default VisibleTodoList