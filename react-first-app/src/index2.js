import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux'
import {Provider} from "react-redux";
import MainComponent from "./components/maincomponent1"
import {rootReduced} from "./store/reducers1";

export const ACTION_CHANGE_FIRST_NAME = 'ACTION_CHANGE_FIRST_NAME';
export const ACTION_CHANGE_SECOND_NAME = 'ACTION_CHANGE_SECOND_NAME';

const store = createStore(rootReduced);

console.log(store.getState());

ReactDOM.render(<Provider store={store}>
	<MainComponent/>
</Provider>, document.getElementById('root'));