import {bindActionCreators} from "redux";
import React from "react";
import {connect} from "react-redux";
import {changeFirstName, changeSecondName} from "../store/actions1";

class Maincomponent1 extends React.Component {
	render() {
		console.log(this.props)
		const {firstName, secondName, changeFirstName, changeSecondName} = this.props;
		return (
			<div>
				<div>
					<input type="text" value={firstName}
					       onChange={(event) => changeFirstName(event.target.value)}
					       placeholder="First name"/>
				</div>
				<div>
					<input type="text" value={secondName}
					       onChange={(event) => changeSecondName(event.target.value)}
					       placeholder="Second name"/>
				</div>
				<div>
					{`${firstName} ${secondName}`}
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	console.log(state);
	return {
		firstName: state.firstName,
		secondName: state.secondName,
	}
};

const putStateToProps = (state) => {
	return {
		firstName: state.firstName,
		secondName: state.secondName
	}
};

const putActionsToProps = (dispatch) => {
	return {
		changeFirstName: bindActionCreators(changeFirstName, dispatch),
		changeSecondName: bindActionCreators(changeSecondName, dispatch)
	}
};

export default connect(mapStateToProps, putActionsToProps)(Maincomponent1);