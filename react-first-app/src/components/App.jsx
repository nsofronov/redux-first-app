import React from "react";
import Registration from "./Registration";
import Auth from "./Auth";
import {Provider} from "react-redux";
import reducers from "../store/reducers";
import {createStore} from "redux";

const store = createStore(reducers)

export default class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<div className="wrapper">
					<h1>Complex State</h1>

					<div className="forms">
						<Auth/>
						<Registration/>
					</div>
				</div>
			</Provider>
		)
	}
}