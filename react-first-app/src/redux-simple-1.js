import {createStore} from 'redux'

const initialState = {
	name: 'Paul',
	seconfName: 'Petrov'
};

function reducer(state = initialState, action) {
	switch (action.type) {
		case 'CHANGE_NAME':
			return {...state, name: action.payload};
		case 'CHANGE_SECOND_NAME':
			return {...state, seconfName: action.payload}
	}
	return state;
}

const store = createStore(reducer);

console.log(store.getState());

const chaneName = {
	type: 'CHANGE_NAME',
	payload: 'Ivan'
};

const changeSecondName = {
	type: 'CHANGE_SECOND_NAME',
	payload: 'Ivanov'
};

store.dispatch(chaneName);

console.log(store.getState());

store.dispatch(changeSecondName);

console.log(store.getState());
