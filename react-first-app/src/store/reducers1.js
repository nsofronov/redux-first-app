import {ACTION_CHANGE_FIRST_NAME, ACTION_CHANGE_SECOND_NAME} from "../index2";

const initialState = {
	firstName: 'Oleg',
	secondName: 'Pavlov'
};

export const rootReduced = (state = initialState, action) => {
	switch (action.type) {
		case ACTION_CHANGE_FIRST_NAME:
			return {...state, firstName: action.payload};
		case ACTION_CHANGE_SECOND_NAME:
			return {...state, secondName: action.payload}
	}
	return state;
};