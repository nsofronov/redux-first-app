import {ADD_TODO, SET_VISIBILITY_FILTER, TOGGLE_TODO, VisibilityFilters} from './actions'

const initialState = {
	visibilityFilter: VisibilityFilters.SHOW_ALL,
	todos: []
};

function visibilityFilter(state = SHOW_ALL, action) {
	switch (action.type) {
		case SET_VISIBILITY_FILTER:
			return action.filter;
		default:
			return state
	}
}

function todos(state = [], action) {
	switch (action.type) {
		case ADD_TODO:
			return {
				...state, todos: [
					...state.todos,
					{
						text: action.text,
						completed: false
					}
				]
			};
		case TOGGLE_TODO:
			return {
				...state, todos: state.todos.map((value, index) => {
					if (index === action.index) {
						return {...value, completed: !value.completed}
					}
					return value;
				})
			};
		default:
			return state
	}
}

function todoApp(state = initialState, action) {
	return {
		...state,
		visibilityFilter: visibilityFilter(state.visibilityFilter, action),
		todos: todos(state.todos, action)
	}
}