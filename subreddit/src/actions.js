import fetch from 'isomorphic-fetch'

//тип действия
export const SELECT_SUBREDDIT = 'SELECT_SUBREDDIT'

//генератор действия
export function selectSubreddit(subreddit) {
	return {
		type: SELECT_SUBREDDIT,
		subreddit
	}
}

export const INVALIDATE_SUBREDDIT = 'INVALIDATE_SUBREDDIT'

export function invalidateSubreddit(subreddit) {
	return {
		type: INVALIDATE_SUBREDDIT,
		subreddit
	}
}

export const REQUEST_POSTS = 'REQUEST_POSTS'
function requestPosts(subreddit) {
	return {
		type: REQUEST_POSTS,
		subreddit
	}
}

export const RECEIVE_POSTS = 'RECEIVE_POSTS'
function receivePosts(subreddit, json) {
	return {
		type: RECEIVE_POSTS,
		subreddit,
		posts: json.data.children.map(child => child.data),
		receivedAt: Date.now()
	}
}

export function fetchPosts(subreddit) {
	return dispatch => {
		dispatch(requestPosts(subreddit))
		return fetch(`http://www.reddit.com/r/${subreddit}.json`)
			.then(response => response.json())
			.then(json => dispatch(receivePosts(subreddit, json)))
	}
}

function shouldFetchPosts(state, subreddit) {
	const posts = state.postsBySubreddit[subreddit]
	if (!posts) {
		return true
	} else if (posts.isFetching) {
		return false
	} else {
		return posts.didInvalidate
	}
}

export function fetchPostsIfNeeded(subreddit) {

	// Помните, что функция также получает getState(),
	// который позволяет вам выбрать, что отправить дальше.

	// Это полезно для того, чтобы избежать сетевого запроса,
	// если доступно закешированное значение.

	return (dispatch, getState) => {
		if (shouldFetchPosts(getState(), subreddit)) {
			// Обратимся у thunk из thunk!
			return dispatch(fetchPosts(subreddit))
		} else {
			// Дадим вызвавшему коду знать, что ждать нечего.
			return Promise.resolve()
		}
	}
}